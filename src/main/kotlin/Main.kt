import MatrixGame.findMiniMaxAndMaxiMinStrategies
import MatrixGame.findSaddlePoints
import MatrixGame.printMatrix

fun main(args: Array<String>) {
    println("Hello World!")
    repeat(100) {
        println("Initializing new matrix:")
        val matrix = MatrixGame.initializeMatrix(3, 3, -4, 5)
        matrix.printMatrix()

        val saddlePoints = matrix.findSaddlePoints()
        if (saddlePoints.isEmpty()) {
            println("No saddle points found")

            val strategies = matrix.findMiniMaxAndMaxiMinStrategies()
            println("MaxiMin: ${strategies.first} Minimax: ${strategies.second} ")
        } else {
            println("Saddle points: ")
            saddlePoints.entries.forEach {
                println(it)
            }
        }
        println("")
    }
}
