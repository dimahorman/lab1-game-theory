object MatrixGame {

    fun Matrix.printMatrix() {
        for (i in this.indicesRows) {
            for (j in this[i]) {
                print(" $j")
            }
            println("")
        }
    }

    fun Matrix.findSaddlePoints(): HashMap<Pair<Int, Int>, Int> {
        val saddlePoints = HashMap<Pair<Int, Int>, Int>()

        for (i in this.indicesRows) {
            var minRow = this[i, 0]
            var columnIndex = 0

            for (j in this.indicesColumns) {
                if (minRow > this[i, j]) {
                    minRow = this[i, j]
                    columnIndex = j
                }
            }

            var maxColumn = this[0, i]
            for (k in this.indicesRows) {
                if (this[k, columnIndex] > maxColumn) {
                    maxColumn = this[k, columnIndex]
                }
            }

            val coordinate = Pair(i, columnIndex)
            if (minRow == maxColumn && !saddlePoints.containsKey(coordinate)) {
                saddlePoints[coordinate] = minRow
            }
        }

        return saddlePoints;
    }

    fun Matrix.findMiniMaxAndMaxiMinStrategies(): Pair<Int, Int> {
        val rowMin = ArrayList<Int>()
        for (i in this.indicesRows) {
            rowMin.add(
                this[i].minOrNull() ?: throw RuntimeException("Unable to find min elem in array elements in array")
            )
        }
        val maxiMin = rowMin.maxOrNull() ?: throw RuntimeException("Unable to find max elem in array in empty array")

        var maxValue = this[0, 0]
        val columnMax = ArrayList<Int>()

        for (j in (0 until this.indicesColumns.last)) {
            for (k in (0 until this.indicesRows.last)) {
                if (maxValue < this[k + 1, j]) {
                    maxValue = this[k + 1, j]
                }
            }
            columnMax.add(maxValue)
            maxValue = this[0, j + 1]
        }
        val miniMax =
            columnMax.minOrNull() ?: throw RuntimeException("Unable to find min elem in array elements in array")

        return Pair(maxiMin, miniMax)
    }

    fun initializeMatrix(
        n: Int,
        m: Int,
        c1: Int,
        c2: Int,
    ): Matrix {
        return Matrix(n, m) { (c1..c2).random() }
    }
}