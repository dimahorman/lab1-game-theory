class Matrix(n: Int,  m: Int, initRow: (Int) -> Int) {
    private val matrixArray: Array<Array<Int>> = Array(n, { Array(m, initRow) })

    val indicesRows: IntRange
        get() = IntRange(0, matrixArray.lastIndex)

    val indicesColumns: IntRange
        get() = IntRange(0, matrixArray.get(0).lastIndex)

    operator fun get(n: Int, m: Int): Int {
        return matrixArray[n][m]
    }

    operator fun get(n: Int): Array<Int> {
        return matrixArray[n]
    }

    operator fun set(n: Int, m: Int, value: Int) {
        matrixArray[n][m] = value
    }
}